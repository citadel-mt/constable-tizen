const path = require('path');

module.exports = {
    entry: path.join(__dirname, 'src/main.js'),
    output: {
        path: path.resolve(__dirname, '../BasicUI/js'),
        filename: "build.js"
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                loader: 'babel-loader'
            },
        ]
    }
};

