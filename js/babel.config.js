module.exports = {
    presets: ['@babel/preset-env'],
    plugins: [
        '@babel/plugin-syntax-dynamic-import',
        '@babel/plugin-syntax-import-meta',
        '@babel/plugin-transform-arrow-functions',
        '@babel/plugin-proposal-throw-expressions',
        '@babel/plugin-proposal-class-properties',
        "@babel/plugin-proposal-private-methods"
    ],
    ignore: ['node_modules', 'dist']
};
