import NotificationManager from "../NotificationManager";

export default class ConnectView {

    #webSocket = undefined;
    #connectedIp = undefined;
    #root = undefined;

    #loading = false;

    constructor(root) {
        this.root = root;
    }

    onMessage = (message) => {
        try {
            const mes = JSON.parse(message.data);
            NotificationManager
                .postNotification(mes.title, mes.text != null ? mes.text : '');
        } catch (e) {}
    };

    onClose = (e) => {
        console.log("On close");
        this.webSocket = undefined;
        this.render();
    };

    onOpen = (data) => {
        console.log("On open");

        this.loading = false;
        this.render();
    };

    onError = (err) => {
        console.log("On error");
        this.webSocket = undefined;
        this.connectedIp = undefined;

        this.loading = false;
        this.render();
    };

    onButConfirmClick = () => {
        const ipInput = document.getElementById("ip_input");

        const webSocketUrl = `ws://${ipInput.value}:12345/`;

        try {
            this.webSocket = new WebSocket(webSocketUrl);

            this.webSocket.onerror = this.onError;
            this.webSocket.onopen = this.onOpen;
            this.webSocket.onmessage = this.onMessage;
            this.webSocket.onclose = this.onClose;

            this.connectedIp = ipInput.value;
            this.loading = true;
            this.render();
        } catch (e) {
            console.log("Connection error");
        }
    };

    onButDisconnectClick = () => {
        if (this.webSocket.readyState === 1) {
            this.webSocket.close();
            this.webSocket = undefined;

            this.render();
        }
    };

    renderConnect = () => {
        const wrapper = document.createElement('div');
        wrapper.setAttribute('class', 'contents');

        const label = document.createElement('label');
        label.innerText = "Enter server's ip:";

        const input = document.createElement('input');
        input.setAttribute('id', 'ip_input');
        input.setAttribute('type', 'text');

        const confirm = document.createElement('button');
        confirm.setAttribute('id', 'but_connect');
        confirm.innerText = "Connect";
        confirm.addEventListener('click', this.onButConfirmClick);

        wrapper.appendChild(label);
        wrapper.appendChild(input);
        wrapper.appendChild(confirm);

        this.root.appendChild(wrapper);
    };

    renderDisconnect = () => {
        const wrapper = document.createElement('div');
        wrapper.setAttribute('class', 'contents');

        const label = document.createElement('label');
        label.innerText = `Connected: ${this.connectedIp}`;

        const disconnect = document.createElement('button');
        disconnect.setAttribute('id', 'but_connect');
        disconnect.innerText = "Disconnect";
        disconnect.addEventListener('click', this.onButDisconnectClick);

        wrapper.appendChild(label);
        wrapper.appendChild(disconnect);

        this.root.appendChild(wrapper);
    };

    renderLoading = () => {
        const wrapper = document.createElement('div');
        wrapper.setAttribute('class', 'contents');

        const label = document.createElement('label');
        label.innerText = "Enter server's ip:";

        const ip = document.createElement('label');
        ip.innerText = this.connectedIp;

        const progress = document.createElement('progress');
        progress.setAttribute('class', 'circle-progress');

        wrapper.appendChild(label);
        wrapper.appendChild(ip);
        wrapper.appendChild(progress);

        this.root.appendChild(wrapper);
    };

    render = () => {
        while (this.root.firstChild) {
            this.root.removeChild(this.root.firstChild);
        }

        if (this.loading) {
            this.renderLoading();
        }

        if (this.webSocket === undefined) {
            this.renderConnect();
        } else {
            this.renderDisconnect();
        }
    }
}
