export default class NotificationManager {

	static get appControl() {
		return new tizen.ApplicationControl('http://tizen.org/appcontrol/operation/create_content',
			null, 'image/jpg', null, null);
	}

	static postNotification(title, body) {
		const notificationGroupDict = {
			/* Notification content */
			content: body,
			actions: {
				/* Device vibrates when the notification is displayed */
				vibration: true,
				/* Application control to be launched when the user selects the notification */
				appControl: NotificationManager.appControl
			}
		};

		const notification = new tizen.UserNotification('SIMPLE', title, notificationGroupDict);

		tizen.notification.post(notification);
	}
}
