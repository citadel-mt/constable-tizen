import ConnectView from './view/ConnectView';

window.onload = function () {
    new ConnectView(
        document.getElementById("root")
    ).render();
};
